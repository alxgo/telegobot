package main

import (
    "encoding/json"
    "errors"
    "flag"
    "fmt"
    "os"
    "os/user"
    "net/http"
    "log"
)

type Bot struct {
    Apikey string `json: "Apikey"`
    Users []int32 `json: "Users"`
}

func (b *Bot) jsonPath() string {
    usr, err := user.Current()

    if err != nil {
        log.Fatal(err)
    }

    return usr.HomeDir + "/.telegobot"
}

func (b *Bot) Load() {
    cfg, err := os.Open(b.jsonPath())
    defer cfg.Close()

    if err != nil {
        log.Fatal(err)
    }

    d := json.NewDecoder(cfg)
    d.Decode(&b)
}

func (b *Bot) getUrl(uid int32, msg string) string {
    return fmt.Sprintf(
        "https://api.telegram.org/bot%s/sendMessage?chat_id=%d&text=%s&parse_mode=markdown",
        b.Apikey,
        uid,
        msg,
    )
}

func (b *Bot) Send(msg string) error {
    c := &http.Client{}

    for _, uid := range b.Users {
        url := b.getUrl(uid, msg)

        r, err := c.Get(url)
        if err != nil {
            return err
        }
        defer r.Body.Close()

        if r.StatusCode != 200 {
            return errors.New("")
        }
    }

    return nil
}

func main() {

    bot := Bot{}
    bot.Load()

    msg := flag.String("msg", "", `Broadcast message`)
    flag.Parse()

    if *msg == "" {
        flag.PrintDefaults()
        os.Exit(0)
    }

    err := bot.Send(*msg)
    if err != nil {
        log.Fatal(err)
    }

    fmt.Println("Message sent.")
}